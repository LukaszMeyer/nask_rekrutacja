#!/usr/bin/python3

# this small script generates many GET requests and sends them to the server,
# which should slow the server down a bit.

import requests
import time

packetsCount = 1000
serverIP = '20.0.0.1'
serverUrlOutput = 'http://' + serverIP + ':5000/output'

timeSum = 0.0;
for i in range(packetsCount):
	startTimestamp = time.time()
	try:
		response = requests.get(serverUrlOutput)
	except requests.exceptions.ConnectionError:
		print('Could not connect to the server - make sure the server is running at ' + serverIP + ':5000')
		exit(-1)
	except:
		print("Unexpected error:", sys.exc_info()[0])
	dt = time.time() - startTimestamp
	timeSum += dt
	#time.sleep(0.001)

avgTime = timeSum / packetsCount
if avgTime > 0.0:
	packetsPerSecond = 1.0 / avgTime
	print('Average requests per second: {}'.format(int(packetsPerSecond)))
#!/usr/bin/python3

import threading
import time
import numpy as np
import json
import logging
from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

useStateSpace = 0
useNeuralNet = 1

if useStateSpace and useNeuralNet:
    print('Select just one object model')
    exit(1)

class IndustrialProcess (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.u = np.array([[0.0]])
        if useStateSpace:
            self.y = np.array([[0.0]])
            self.x = np.array([[1.0], [2.0], [0.0]])
            self.b = np.array([[0.05309], [-0.07041], [0.02291]])
            self.A = np.array([[2.768, 1.0, 0.0], [-1.947, 0.0, 1.0], [0.4066, 0.0, 0.0]])
            self.C = np.array([[1.0, 0.0, 0.0]])
        elif useNeuralNet:
            self.u_v = np.array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
            self.y = np.array([[0.0], [0.0], [0.0]])
            # load N neural net paramters
            self.w1 = np.loadtxt(open("NeuralNetData/w1.txt", "rb"), delimiter="\t")
            self.w2 = np.loadtxt(open("NeuralNetData/w2.txt", "rb"), delimiter="\t")
            self.w10 = np.loadtxt(open("NeuralNetData/w10.txt", "rb"), delimiter="\t")
            self.w20 = np.loadtxt(open("NeuralNetData/w20.txt", "rb"), delimiter="\t")
            self.w10 = self.w10.reshape(-1,1)

    def neuralNetUpdate(self):
        # read new input (shift u)
        self.u_v = np.vstack((self.u, self.u_v[0:5]))
        # prepare NN input (q vector)
        q = np.vstack((self.u_v[4:6], self.y[0:2]))
        # calculate NN output
        new_y = self.calculateNeuralNetOutput(q)
        # shift y
        self.y = np.vstack((new_y, self.y[0:2]))
        print('q:')
        print(q)
        print('u_v:')
        print(self.u_v)
        print('y:')
        print(self.y)

    def stateSpaceUpdate(self):
        self.x = np.matmul(self.A, self.x) + np.matmul(self.b, self.u)
        self.y = np.matmul(self.C, self.x)
        print('%s Process update' % time.ctime(time.time()))
        print('x:')
        print(self.x)
        print('u:')
        print(self.u)
        print('y:')
        print(self.y)


    def run(self):
        while True:
            # TODO lock here
            if useStateSpace:
                self.stateSpaceUpdate()
            elif useNeuralNet:
                self.neuralNetUpdate()
            else:
                print('PLEASE SELECT STATE SPACE MODEL OR NEURAL NET MODEL')
            # TODO unlock here
            time.sleep(0.1);

    def setNewInput(self, newInput):
        # TODO lock
        self.u = newInput
        # TODO unlock

    def getOutput(self):
        # TODO lock
        output = self.y
        # TODO unlock
        return output

    def getState(self):
        # TODO lock
        state = self.x
        # TODO unlock
        return state

    def calculateNeuralNetOutput(self, q):
        tmp = np.tanh(self.w10 + np.matmul(self.w1, q))
        y = self.w20 + np.matmul(self.w2, tmp)
        return y


class ProcessInput(Resource): # used to set U(k)
    def get(self):
        return "Input cannot be read", 400

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("value")
        args = parser.parse_args()
        valueString = args["value"]
        newInputValue = np.array(json.loads(valueString)) # TODO add exception handling here
        # TODO lock here
        processThread.setNewInput(newInputValue)
        # TODO unlock
        return "Input changed succesfully", 200

class ProcessOutput(Resource): # used to read Y(k)
    def get(self):
        return processThread.getOutput().tolist(), 200 # getOutput() returns numpy array, we need to convert it into a format that JSON encoder can undestand

class ProcessState(Resource): # used to read x
    def get(self):
        if useStateSpace:
            return processThread.getState().tolist(), 200 # getState() returns numpy array, we need to convert it into a format that JSON encoder can undestand
        else:
            return "Cannot read process state", 400

api.add_resource(ProcessInput, "/input")
api.add_resource(ProcessOutput, "/output")
api.add_resource(ProcessState, "/state")

processThread = IndustrialProcess()
processThread.start();

app.run(host="20.0.0.1", port = int(5000), debug=False, use_reloader=False)
#app.run(host="127.0.0.1", port = int(5000), debug=True, use_reloader=False)

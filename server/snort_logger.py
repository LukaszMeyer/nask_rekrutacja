#!/usr/bin/python3

from pymongo import MongoClient
import pprint
import subprocess
import json
import sys

# parse mongoDB server address and port
host = ""
dbPort = 0

if len(sys.argv) == 2:
	dbPort = 27017
elif len(sys.argv) == 3:
	dbPort = int(sys.argv[2])
else:
	print("ERROR\n Usage: " + sys.argv[0] + " < mongoDB server IP address > [ Port ]")
	sys.exit(1)
dbIP = sys.argv[1]

class SnortLogger():
	def __init__(self):
		self.client = MongoClient(dbIP, dbPort)
		self.db = self.client.snort_log
		self.icmp_collection = self.db.icmp_events
		self.dos_collection = self.db.dos_events
		self.other_collection = self.db.other_events
		self.proc = subprocess.Popen(["bash u2json.sh"], shell=True, stdout=subprocess.PIPE)

	def saveAlertToDatabase(self, jsonEntry):
		if jsonEntry["event"]["signature-id"] == 10000001: # ICMP event
			# add to ICMP events database
			print("ICMP event")
			newEntry = self.icmp_collection.insert_one(jsonEntry)
			print(newEntry.inserted_id)
		elif jsonEntry["event"]["signature-id"] == 1000000: # DoS event
			print("DoS event")
			# add to DoS events database
			newEntry = self.dos_collection.insert_one(jsonEntry)
			print(newEntry.inserted_id)
		else: # other event
			print("Other event")
			# add to other events database
			newEntry = self.other_collection.insert_one(jsonEntry)
			print(newEntry.inserted_id)

	def run(self):
		while self.proc.poll() is None:
			output = self.proc.stdout.readline()
			line = output.decode("utf-8")
			print(line)
			try:
				newEntry = json.loads(line)
				print(newEntry["type"])
				if newEntry["type"] == "event": # new event
					self.saveAlertToDatabase(newEntry)
			except Exception as e:
				print(e)
				print("Parsing new line failed")

logger = SnortLogger()
logger.run()


#!/usr/bin/python3

import requests
import numpy as np
import time
import threading
import json
from flask import Flask
from flask_restful import Api, Resource, reqparse
from simple_pid import PID


useNeuralNetProcess = 1
controllerMutex = threading.Lock()

class Controller (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.serverIP = '20.0.0.1'
        #self.serverIP = '127.0.0.1'
        self.urlOutput = 'http://' + self.serverIP + ':5000/output'
        self.urlState = 'http://' + self.serverIP + ':5000/state'
        self.urlInput = 'http://' + self.serverIP + ':5000/input'
        self.y = np.array([0.0])
        self.y_sp = np.array([1.0])
        self.x = np.array([[0.0], [0.0], [0.0]])
        self.K = np.array([[100.19, 32.88, -32.15]])
        if useNeuralNetProcess:
            self.pid = PID(0.5, 0.1, 0.1, setpoint=self.y_sp, sample_time = 0.1)
        else:
            self.pid = PID(0, 30.0, 0, setpoint=self.y_sp, sample_time = 0.1)

    def getProcessOutput(self): # returns numpy array
        response = requests.get(self.urlOutput)
        return np.array(response.json(), dtype='float64')

    def getProcessState(self): # returns numpy array
        response = requests.get(self.urlState)
        return np.array(response.json(), dtype='float64')

    def setSetpoint(self, newSetpoint):
        if controllerMutex.acquire(True, 0.1):
            self.y_sp = newSetpoint
            self.pid.setpoint = newSetpoint
            print(self.y_sp)
            controllerMutex.release()
            return True
        else:
            raise Exception('Could not lock the mutex')

    def setProcessInput(self, value): # value is of np.array type
        data = json.dumps(value.tolist())
        payload = {'value': data}
        response = requests.post(self.urlInput, params=payload)

    def run(self):
        i = 0
        while True:
            try:
                self.y = self.getProcessOutput()
                if useNeuralNetProcess != 1:
                    self.x = self.getProcessState();
                    u_ss = -np.matmul(self.K, self.x)
                    u = self.pid(self.y[0]) + u_ss
                else:
                    u = -self.pid(self.y[0])
                    if u < -1.0:
                        u = -1.0
                    elif u > 1.0:
                        u = 1.0
                self.setProcessInput(np.array(u))
                print('---------------')
                #print(self.x)
                print('y[0]:')
                print(self.y[0])
                print('u:')
                print(u)
            except requests.exceptions.ConnectionError:
                print('Could not connect to the server')
            except:
                print("Unexpected error:", sys.exc_info()[0])
            time.sleep(0.1)

class ControllerSetpoint(Resource): # used to set y_SP

    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument("value")
            args = parser.parse_args()
            valueString = args["value"]
            newSetpoint = np.array(json.loads(valueString)) # TODO add exception handling here
            print(type(newSetpoint))
            ret = controllerThread.setSetpoint(newSetpoint)
        except:
            print("Error while setting new setpoint")
            return "Error while setting new setpoint", 400
        return "Setpoint changed succesfully", 200

app = Flask(__name__)
api = Api(app)

controllerThread = Controller()
controllerThread.start()

api.add_resource(ControllerSetpoint, "/setpoint") # used to set y_SP
app.run(host="10.0.0.5", port=int(5001), debug=True, use_reloader=False)
#app.run(host="127.0.0.1", port=int(5001), debug=True, use_reloader=False)

# change remote_user_name in group_vars/DCS_SERVER and group_vars/DCS_CLIENT to appropriate name. The user should be in sudoers group.
# remember to properly setup inventory file (/etc/ansible/hosts)
- name: install packages
  remote_user: "{{remote_user_name}}"
  hosts: DCS_SERVER, DCS_CLIENT
  tasks:
    - name: install required packages
      become: true
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
        - git
        - python3
        - python3-pip
        #- python3-venv

    - name: install required Python packages
      pip:
        executable: pip3
        #virtualenv: /tmp/py3
        #virtualenv_command: /usr/bin/python3 -m venv
        name:
          - numpy
          - flask
          - flask_restful
          - simple_pid
          - pymongo

    - name: Clone nask_rekrutacja repository
      git:
        repo: https://LukaszMeyer@bitbucket.org/LukaszMeyer/nask_rekrutacja.git
        dest: "~/nask_rekrutacja"
        clone: yes
        recursive: yes
        version: master

# below script is based on https://github.com/NikhilaSattala/snort-ansible
- name: install and setup Snort
  remote_user: "{{remote_user_name}}"
  hosts: DCS_SERVER
  tasks:
    - name: Install Snort pre-requisites
      become: yes
      apt:
        name: "{{packages}}"
        state: present
      vars:
        packages:
          - "build-essential"
          - "libpcap-dev"
          - "libpcre3-dev"
          - "libdumbnet-dev"
          - "bison"
          - "flex"
          - "zlib1g-dev"
          - "liblzma-dev"
          - "openssl"
          - "libssl-dev"
          - "rsync"

    - name: Create Snort source directory
      become: yes
      file: path=/opt/snort-src state=directory

    - name: Download DAQ
      become: yes
      get_url:
        url: https://www.snort.org/downloads/snort/daq-{{daq_version}}.tar.gz
        dest: /tmp/
      register: download_daq

    - name: Untar DAQ
      become: yes
      unarchive: src=/tmp/daq-{{daq_version}}.tar.gz
                 dest=/opt/snort-src
                 copy=no
      when: download_daq.changed
      register: untar_daq

    - name: Configure DAQ
      command: chdir=/opt/snort-src/daq-{{daq_version}} {{item}}
      become: yes
      with_items:
      - "./configure"
      - "/usr/bin/make"
      - "/usr/bin/make install"
      when: untar_daq.changed

    - name: Download Snort
      get_url: 
        url: https://snort.org/downloads/archive/snort/snort-{{snort_version}}.tar.gz
        dest: /tmp
      register: download_snort

    - name: Untar Snort
      become: yes
      unarchive: src=/tmp/snort-{{snort_version}}.tar.gz
                 dest=/opt/snort-src
                 copy=no
      when: download_snort.changed
      register: untar_snort

    - name: Install Snort
      command: chdir=/opt/snort-src/snort-{{snort_version}} {{item}}
      become: yes
      with_items:
      - "./configure --enable-sourcefire"
      - "/usr/bin/make"
      - "/usr/bin/make install"
      - "ldconfig"
      when: untar_snort.changed
      register: install_snort

    - name: Create Snort symlink
      become: yes
      file: src=/usr/local/bin/snort dest=/usr/sbin/snort state=link

    - name: Create Snort group 
      become: yes
      group: name=snort state=present 

    - name: Create Snort user
      become: yes
      user: name=snort
            group=snort
            system=yes
            shell=/sbin/nologin
            comment=SNORT_IDS

    - name: Create Snort directories
      become: yes
      file: path=/etc/{{item}} state=directory owner=snort group=snort mode=5775 
      with_items:
      - snort
      - snort/rules
      - snort/rules/iplists
      - snort/preproc_rules
      - snort/so_rules
      register: dir_created

    - name: Create Snort dynamic rules directory
      become: yes
      file: path=/usr/local/lib/snort_dynamicrules state=directory owner=snort group=snort mode=5775

    - name: Create Snort log directories
      become: yes
      file: path=/var/log/{{item}} state=directory owner=snort group=snort mode=5775 
      with_items:
      - snort
      - snort/archived_logs

    - name: Create Files that stores Rules and IP lists
      become: yes
      file: path=/etc/snort/{{item}} state=touch owner=snort group=snort mode=775
      with_items:
      - rules/iplists/black_list.rules
      - rules/iplists/white_list.rules
      - rules/local.rules
      - sid-msg.map
      when: dir_created.changed

    - name: Copy Config files and dynamic preprocessors
      become: yes
      command: cp /opt/snort-src/snort-{{snort_version}}/etc/{{item}} /etc/snort
      with_items:
      - file_magic.conf
      - reference.config
      - threshold.conf
      - classification.config
      - unicode.map
      - gen-msg.map
      - attribute_table.dtd
      when: install_snort.changed 

    - name: Copy dynamic preprocessors
      become: yes
      synchronize:
         src: /opt/snort-src/snort-{{snort_version}}/src/dynamic-preprocessors/build/usr/local/lib/snort_dynamicpreprocessor/ 
         dest: /usr/local/lib/snort_dynamicpreprocessor
      delegate_to: "{{ inventory_hostname }}"
      when: install_snort.changed

    # - name: Snort Startup script
    #   become: yes
    #   template:
    #     src: "../templates/etc/init/snort.conf.j2"
    #     dest: "/etc/init/snort.conf"
    #     owner: snort
    #     group: snort
    #     mode: 0755

    - name: Copy modified threshold config file
      become: yes
      command: cp /home/{{remote_user_name}}/nask_rekrutacja/snort_config/{{item}} /etc/snort
      with_items:
      - threshold.conf
      - snort.conf
      # when: install_snort.changed

    - name: Copy custom rules
      become: yes
      command: cp /home/{{remote_user_name}}/nask_rekrutacja/snort_config/rules/{{item}} /etc/snort/rules
      with_items:
      - simple_ping.rules
      - simple_dos.rules
      when: install_snort.changed

    - name: Set snort.conf ownership
      become: yes
      file:
        path: "/etc/snort/snort.conf"
        owner: snort
        group: snort
        mode: '0755'
#Detekcja prostych ataków w środowisku rozproszonego systemu sterowania

##** Serwer **
Na serwerze (maszyna DCS_SERVER, program `server/server.py`) zaimplementowałem dwa różne obiekty:

 * Proces reprezentowany jako liniowy obiekt w przestrzeni stanów, 3-go rzędu, SISO, niestabilny. Wykorzystałem model analizowany w projekcie realizowanym w czasie studiów na przedmiocie Sterowanie Procesami (dokumentacja w pliku `Dokumenty/STP_dokumentacja.pdf`)
 * Proces nieliniowy dynamiczny reprezentowany przez sieć neuronową typu MLP (*Multi Layer Perceptron*). Sieć ma jedną warstwę ukrytą i zastosowano w niej funkcje tangensa hyperbolicznego. Bazowałem na projekcie wykonywanym wcześniej razem z Arkiem Piórkowskim (w ramach przedmiotu Sztuczna Inteligencja w Automatyce). Warstwa ukryta ma 8 neuronów. Dokładne równania  opisujące symulowany proces opisane są w załączonej dokumentacji (`Dokumenty/SZAU_dokumentacja.pdf`)

Wybór symulowanego procesu odbywa się przez odpowiednie ustawienie w programie wartości zmiennych `useStateSpace` i `useNeuralNet` (wartość 0 lub 1).

Wybrane metody pozwalają na pewną elastyczność przy kształtowaniu charakterystyki obiektu.

 * W przypadku modelu w przestrzeni stanów możliwe jest zmienianie macierzy A i wektorów b, C (w chwili obecnej przez przypisanie odpowiednich wartości w kodzie, ale można też dodać odpowiedni interfejs REST API i przesyłanie tych macierzy w postaci JSON). Potencjalnie możliwa jest też zmiana liczby wejść i wyjść obiektu (nie implementowałem ze względu na ograniczony czas).
  * Parametry sieci neuronowej (wagi) wczytywane są z plików CSV umieszczonych w folderze `server/NeuralNetData`. Można dodać REST API, aby te wagi można było ustawiać 'on-line', w czasie działania programu.

Inne metody pozwalające na kształtowanie charakterystyki obiektu (niezaimplementowane):

 * model obiektu oraz zakłóceniowy w postaci transmitancji z czasem ciągłym lub dyskretnym, przez REST API przesyłane współczynniki wielomianów tych transmitancji
 	- SISO albo MIMO, liniowy
 	- Możliwość uwzględnienia opóźnienia
 	- konieczna dyskretyzacja po stronie serwera jeśli przesyłany opis  modelu z czasem ciągłym
  * model obiektu oraz zakłóceniowy w postaci sieci neuronowej - można utworzyć model z wykorzystaniem na przykład biblioteki TensorFlow i [zapisać go do pliku](https://www.tensorflow.org/beta/guide/keras/saving_and_serializing#whole-model_saving), a następnie przesłać ten plik jako zapytanie POST do serwera. Metoda zbliżona do zaimplementowanej, ale bardziej uniwersalna (można np. zmieniać strukturę sieci).

Komunikacja serwera ze światem zewnętrznym (regulatorem) odbywa się przez REST API (port 5000). Niestety nie zdążyłem zapoznać się ze Swagger UI, więc tutaj dodam krótki opis endpointów.
API serwera ma trzy endpointy:

  * `/input` - przyjmuje zapytania POST, umożliwia zadanie wartości wejściowej.
  * `/output` - przyjmuje zapytania GET, umożliwia odczytanie wartości wyjściowej procesu.
  * `/state` - umożliwia odczytanie stanu (wektora x) jeżeli jest aktywny model w przestrzeni stanów (wykorzystywane przez regulator od stanu)
  
  Podczas rozwoju i testów oprogramowania wykorzystywałem narzędzie [Insomnia](https://insomnia.rest/), które pozwala na wygodne manualne wysyłanie zapytań do REST API.
 
##** Klient **
 Dla klienta utworzono maszynę wirtualną DCS_CLIENT z systemem Debian 10.
 Na tej maszynie uruchamiana jest aplikacja w Pythonie, która poprzez zapytania GET do REST API uruchomionego na serwerze może odczytać aktualną wartość wyjściową procesu, oraz przez zapytanie POST ustawić wartość wejściową procesu. Do generacji i wysyłania zapytań wykorzystano paczkę [requests](https://2.python-requests.org//pl/latest/).
 Klient umożliwia też ustawienie wartości zadanej dla regulatora poprzez REST API (port 5001) - endpoint `/setpoint`.
 W czasie testów do ręcznego generowania zapytań REST wykorzystano program [Insomnia](https://insomnia.rest/).
 
 Jeżeli na serwerze uruchomiony jest model w przestrzeni stanów, to zmienną `useNeuralNetProcess` trzeba ustawić na 0, a jeśli na serwerze uruchomiono model neuronowy, to na wartość 1.
 
 Zaimplementowano dwa rodzaje regulacji:
 
   * regulator ze sprzężeniem od stanu + człon I - wykorzystywany do sterowania obiektu reprezentowanego przez model w przestrzeni stanów. Jest to rozwiązanie niezbyt eleganckie. Regulator ze sprzężeniem od stanu służy głównie do stabilizacji obiektu (i sprowadza stan do zera), a człon całkujący (regulatora PID) służy do powolnego osiągnięcia wartości zadanej. Nastawy regulatora ze sprzężeniem od stanu dobrane metodą lokowania biegunów (z_1 = z_2 = z_3 = 0.5), opisane w załączonej dokumentacji z przedmiotu STP.
   * regulator PID - wykorzystany jest do regulacji działania modelu neuronowego.
Do powyższych regulatorów nie implementowałem metody przestrajania 'on-line' (ograniczony czas), ale można oczywiście zaimplementować taki mechanizm przez REST API.
Inne algorytmy regulacji, które potencjalnie można zaimplementować to np. różnego rodzaju algorytmy predykcyjne - DMC, z nieliniową optymalizacją itd.
 
##** Konfiguracja maszyn wirtualnych **
 Do wirtualizacji zastosowałem [VirtualBox 6.0.8 r130520](https://www.virtualbox.org/wiki/Download_Old_Builds_6_0). Jak wspomniano wcześniej, uruchamiane są dwie maszyny wirtualne: DCS_CLIENT i DCS_SERVER.
 
Skonfigurować trzeba  adaptery sieciowe (File ->Host Network Manager). Należy dodać dwa adaptery o nazwach `vboxnet0` i `vboxnet1` i skonfigurować je jak na rysunkach:
![ ](Grafiki/config_vboxnet0.png  "Konfiguracja vboxnet0")
![ ](Grafiki/config_vboxnet1.png  "Konfiguracja vboxnet1")

Po utworzeniu maszyn wirtualnych w ich ustawieniach trzeba przypisać utworzone adaptery: PPM na wybranej maszynie -> Settings -> Network -> w zakładce 'Adapter 1' w Name wybrać odpowiednio 'vboxnet0' dla DCS_SERVER i 'vboxnet1' dla DCS_CLIENT.
 
##** Konfiguracja sieci **
 
Plik `/etc/network/interfaces` dla DCS_SERVER:
```
source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3
iface enp0s3 inet static
address 20.0.0.1
netmask 255.255.255.0

auto enp0s8
iface enp0s8 inet dhcp
```

Plik `/etc/network/interfaces` dla DCS_CLIENT:
```
source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3
iface enp0s3 inet static
address 10.0.0.5
netmask 255.255.255.0

auto enp0s8
iface enp0s8 inet dhcp
```

Aby maszyny DCS_SERVER i DCS_CLIENT miały wymagane adresy IP (odpowiednio 20.0.0.1 i 10.0.0.5) utworzone zostały w VirtualBox-ie dwa host network adaptery: vboxnet0 (przypisany do DCS_SERVER) i  vboxnet1 (przypisany do DCS_CLIENT) - odpowiednio o adresach 20.0.0.2 i 10.0.0.2.

Próbowałem uruchomić komunikację pomiędzy tymi maszynami poprzez dodawanie odpowiednich reguł routowania na hoście, ale komunikacji nie udało się uruchomić. (dodawane reguły to  `route add -net 20.0.0.0 netmask 255.255.255.0 gw 20.0.0.2` oraz  `route add -net 10.0.0.0 netmask 255.255.255.0 gw 10.0.0.2`)

Dodałem do każdej maszyny drugi adapter NAT (którego potrzebowałem, aby instalować paczki z Internetu) i komunikacja między maszynami zaczęła działać. Trochę niejasne jest dla mnie, jak dokładnie to zadziałało, ale niestety nie miałem czasu, żeby się zagłębiać w ten temat.

Trasa pakietu z serwera do klienta:

```
traceroute to 10.0.0.5 (10.0.0.5), 30 hops max, 60 byte packets
 1  _gateway (10.0.3.2)  0.117 ms  0.085 ms  0.081 ms
 2  10.0.0.5 (10.0.0.5)  0.398 ms  0.394 ms  0.418 ms
```


Trasa pakietu z klienta do serwera:

```
traceroute to 20.0.0.1 (20.0.0.1), 30 hops max, 60 byte packets
 1  _gateway (10.0.3.2)  0.148 ms  0.126 ms  0.135 ms
 2  20.0.0.1 (20.0.0.1)  0.347 ms  0.265 ms  0.296 ms
```

Wygląda na to, że pakiety przekazywane są do gateway-a NAT-owego (10.0.3.2).

 
## ** Uruchomienie serwera i klienta **
### Czynności wykonywane ręcznie ###

Czynności wykonywane ręcznie na 'czystej' maszynie (Debian 10):

* instalacja openssh-server
* konfiguracja statycznych IP (opisanej wyżej) 
* dodanie użytkownika do grupy sudoers [LINK](https://unix.stackexchange.com/questions/292562/adding-a-sudoer-in-debian)
* skopiowanie klucza publicznego z hosta na maszynę (na hoście: `ssh-copy-id username@IP_address`)

Reszta konfiguracji z wykorzystaniem skryptu Ansible.

### Przygotowanie środowiska ###
Do automatycznego przygotowania środowiska utworzyłem skrypt Ansible (pliki w folderze `ansible`). Zakładam, że w pliku `/etc/ansible/hosts` wpisane są odpowiednie nazwy serwerów i ich adresy IP. Skrypt testowałem na Ansible w wersji 2.8.2.

Uruchomienie skryptu:
`ansible-playbook setup.yml --ask-become-pass`

Po uruchomieniu maszyn  na serwerze (maszyna DCS_SERVER) trzeba wykonać komendy
`cd ~/nask_rekrutacja/server`
`python3 server.py`

Po stronie klienta (DCS_CLIENT) komendy
`cd ~/nask_rekrutacja/client`
`python3 client.py`

 
## ** Snort i wykrywanie ataków**
 Na maszynie DCS_SERVER zainstalowany został Snort i utworzona została bardzo prosta reguła umożliwiająca wykrycie ataku DoS (zakładam, że atakujący zna adres IP serwera oraz port, na którym nasłuchuje aplikacja symulująca działanie obiektu).
Reguła (plik /etc/snort/rules/simple_dos.rules):
`alert tcp any any -> $HOME_NET 5000 (flags: S; msg:"Possible TCP DoS"; flow: stateless; detection_filter: track by_src, count 200, seconds 10; sid:1000000;rev:1;)`

Do testów dodano też regułę umożliwiającą wykrycie 'pingowania' serwera:
`alert icmp any any -> $HOME_NET any (msg:"ICMP test detected"; GID:1; sid:10000001; rev:001; classtype:icmp-event;)`

Snort miał być zainstalowany na oddzielnej maszynie wirtualnej, ale pozwoliłem sobie uprościć sytuację i go zainstalowałem na DCS_SERVER. Przekazywanie ruchu sieciowego w docelowej aplikacji (tzn. najprawdopodobniej oddzielne komputery w jednej podsieci) można zrealizować poprzez odpowiednie ustawienie switcha/routera.
**Uruchamianie Snort-a**: `bash ~/nask_rekrutacja/server/run_snort.sh`

Nie tworzyłem już kolejnej maszyny wirtualnej dla atakującego, tylko symulacje ataków przeprowadzałem z wykorzystaniem hosta (Ubuntu 16 w moim przypadku).
Ataki generowałem z wykorzystaniem narzędzi: hping3, [PyFlooder](https://github.com/D4Vinci/PyFlooder) oraz własny program `mini_requests_flood.py`.
Komenda dla hping3:
`sudo hping3 -S -p 5000 --flood 20.0.0.1`

(dodanie flagi `--rand-source` do polecenia sprawia, że wykrywanie ataku nie działa, ale jest to zgodne z oczekiwaniem, bo reguła Snort-a jest ustawiona jako `track by_src`)
Komenda dla PyFlooder:
`python2 pyflooder.py 20.0.0.1 5000 10000`

#### Własny program do atatku wolumetrycznego ####

Utworzony został prosty skrypt w Python 3 (`attacker/mini_requests_flood.py`), który generuje wiele zapytań GET do serwera (odczytuje stan wyjścia procesu). Generowany ruch jest wykrywany przez Snort-a jako atak DoS.

Próbowałem też utworzyć program wykorzystujący bibliotekę [scapy](https://scapy.readthedocs.io/en/latest/index.html), ale z jakiegoś powodu nie udało się uzyskać wykrywania pakietów wysyłanych na port 5000 jako atak DoS (chociaż pakiety ICMP były poprawnie wykrywane przez Snort-a). 

Niezależnie od wykorzystanego programu, po stronie serwera pojawiają się alerty Snort-a (zakładając, że podczas uruchamiania Snort-a została przekazana flaga `-A console`):
`07/17-00:45:47.923259  [**] [1:1000000:1] Possible TCP DoS [**] [Priority: 0] {TCP} 20.0.0.2:22919 -> 20.0.0.1:5000`

W pliku `/etc/snort/threshold.conf` dodano filtry, dzięki którym alerty od utworzonych reguł DoS i ICMP są logowane z odstępem 10 sekund:
```
# simple_dos
 event_filter gen_id 1, sig_id 1000000, type limit, \
     track by_dst, count 1, seconds 10
# simple ICMP
 event_filter gen_id 1, sig_id 10000001, type limit, \
     track by_dst, count 1, seconds 10
```

Oczywiście mając uruchomiony program `server.py` w outpucie pojawia się mnóstwo 'śmieciowych' odbieranych pakietów, ale dopóki atakujący nie zna struktury oczekiwanych danych, to 'jedynym' problemem jest wykorzystanie CPU w 100% do parsowania otrzymywanych danych.

Jeżeli przyjmiemy, że atakujący wie, jakiego typu danych oczekuje serwer (zapytanie POST z odpowiednimi polami danych), to możliwe jest, że atakujący wpłynie na zachowanie obiektu (poprzez odpowiednie sterowanie wartością wejściową). Prostą metodą zabezpieczającą przed powyższym jest dodanie pola typu 'magic' do zapytania POST (np. jakiś ciąg znaków) znany tylko klientowi i serwerowi. Jeżeli serwer stwierdzi, że zawartość tego pola nie jest poprawna, to odrzuca zapytanie. Oczywiście na pewno są też bardziej wyrafinowane metody (jakieś mechanizmy autoryzacji, metody kryptograficzne itd.).

W opisie zadania podano, że należy utworzyć program, który alerty od Snort-a oraz zablokuje nieporządany ruch.
Nie zostało to zaimplementowane ze względu na brak czasu. Pomysły na zrealizowanie tej funkcji:

- wykorzystanie wtyczki [SnortSam](http://www.snortsam.net/), która podobno pozwala na blokowanie IP poprzez IPtables. Niestety wtyczka nie jest już rozwijana i utrzymywana, więc trzeba by zweryfikować, czy działa z najnowszym Snort-em (najnowsza wersja to 2.9.13.0, a ostatni patch SnortSam-a jest do wersji 2.9.5.3)

- utworzenie dedykowanego programu (np. w Python), który będzie automatycznie na bieżąco wczytywał logi z pliku i z alertu będzie wyciągany adres IP atakującego, który będzie blokowany z wykorzystaniem iptables   (wykonanie polecenia `iptables -A INPUT -s IP_ADDR -j DROP`)
 
 - koncepcja jak powyżej, ale Snort zapisuje alerty do bazy danych, a utworzony program odczytuje je na bieżąco i dodaje odpowiednie reguły do iptables

Jeżeli Snort uruchomiony byłby na maszynie innej niż serwer, to do powyższych metod trzeba by dodać komunikację z firewallem/routerem (przesyłać do niego adres IP, który ma zostać zablokowany). 

### **Zapisywanie alertów do bazy danych**

Utworzono program `snort_logger.py`, który uruchamiany jest na tej samej maszynie co Snort. Program na bieżąco odczytuje logi Snort-a, konwertuje je z formatu unified2 na JSON i wpisy z alertami zapisuje do bazy MongoDB, która uruchomiona jest na oddzielnej maszynie.

Przy uruchamianiu `snort_logger.py` jak argument trzeba podać adres IP i opcjonalnie port serwera MongoDB, na przykład:
`python3 snort_logger.py 192.168.0.15`

Konwertowanie unified2 do JSON realizowane jest z wykorzystaniem [py-idstools](https://github.com/jasonish/py-idstools) - dodane jako submodule do tego repozytorium.

*Sidenote: MongoDB uruchomiłem na oddzielnej maszynie (Ubuntu16), bo nie ma jeszcze oficjalnej paczki MongoDB na Debiana 10.*

Alerty zapisywane są do bazy o nazwie `snort_log`, gdzie tworzone są trzy kolekcje: `dos_events`, `icmp_events` i `other_events`. Zapisywane są wszystkie dane logowane przez Snort-a.

Do podglądania zawartości bazy danych wykorzystałem graficzne narzędzie [Robo 3T](https://robomongo.org/).